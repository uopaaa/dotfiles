;;load custom
(setq custom-file "~/.emacs.d/emacs-custom.el")
(load custom-file)

;;UTF-8 as default encoding
(set-language-environment "UTF-8")
(set-default-coding-systems 'utf-8-unix)

;;font
(set-frame-font "Iosevka-14" nil t)

(setq-default initial-scratch-message nil)

(setq find-file-visit-truename t)
    
(require 'package)
(add-to-list 'package-archives
             '("melpa-stable" . "https://stable.melpa.org/packages/"))

;; All the icons for emacs
(use-package all-the-icons
  :if (display-graphic-p))

(use-package doom-modeline
  :ensure t
  :config
    (setq doom-modeline-height 14)

  :init (doom-modeline-mode 1))

(add-hook 'dired-mode-hook 'all-the-icons-dired-mode)

(use-package all-the-icons-ibuffer
  :ensure t
  :config
    ;; Whether display the icons.
    (setq all-the-icons-ibuffer-icon t) 

    ;; Whether display the colorful icons.
    ;; It respects `all-the-icons-color-icons'.
    (setq all-the-icons-ibuffer-color-icon t)

    ;; The default icon size in ibuffer.
    (setq all-the-icons-ibuffer-icon-size 1.0)

    ;; The default vertical adjustment of the icon in ibuffber.
    (setq all-the-icons-ibuffer-icon-v-adjust 0.0)
    
    ;; Use human readable file size in ibuffer.
    (setq  all-the-icons-ibuffer-human-readable-size t)
  :init (all-the-icons-ibuffer-mode 1))


;; Dashboard
(use-package dashboard
  :ensure t
  :config
  (dashboard-setup-startup-hook))
    
;;indent to 2 spaces on java files
(add-hook 'java-mode-hook (lambda ()
                            (setq c-basic-offset 2
                                  tab-width 2
                                  indent-tabs-mode t)))

;; Change default indent to spaces, and let this be 4 spaces long
;;(setq-default indent-tabs-mode nil)
;;(setq-default tab-width 4)
;;(setq indent-line-function 'insert-tab)

;;load theme
(load-theme 'base16-material-palenight t)

;;company mode :)

(add-hook 'after-init-hook 'global-company-mode)

;; Save all tempfiles in $TMPDIR/emacs$UID/                                                        
(defconst emacs-tmp-dir (expand-file-name (format "emacs%d" (user-uid)) temporary-file-directory))
(setq backup-directory-alist
      `((".*" . ,emacs-tmp-dir)))
(setq auto-save-file-name-transforms
      `((".*" ,emacs-tmp-dir t)))
(setq auto-save-list-file-prefix
      emacs-tmp-dir)

;;enable line numbering in all buffersx
;;(global-linum-mode 1)

;;disable menubar, topbar and scrollbar
(setq inhibit-startup-message t)
(menu-bar-mode -1)
(toggle-scroll-bar -1)
(tool-bar-mode -1)

;;load ido mode
(ido-mode 1)
;;show any name that has the chars you typed
(setq ido-enable-flex-matching t)
;; use current pane for newly opened file
(setq ido-default-file-method 'selected-window)
;; use current pane for newly switched buffer
(setq ido-default-buffer-method 'selected-window)
;; stop ido from suggesting when naming new file
(define-key (cdr ido-minor-mode-map-entry) [remap write-file] nil)

;;resize better
(global-set-key (kbd "S-C-<left>") 'shrink-window-horizontally)
(global-set-key (kbd "S-C-<right>") 'enlarge-window-horizontally)
(global-set-key (kbd "S-C-<down>") 'shrink-window)
(global-set-key (kbd "S-C-<up>") 'enlarge-window)

;;org kbs
(global-set-key (kbd "C-c l") #'org-store-link)
(global-set-key (kbd "C-c a") #'org-agenda)
(global-set-key (kbd "C-c c") #'org-capture)
(setq org-log-done t)

(setq org-agenda-files (list "~/org/work.org"
                             "~/org/school.org" 
                             "~/org/home.org"))

(add-hook 'org-mode-hook '(lambda () (setq fill-column 80)))
(add-hook 'org-mode-hook 'turn-on-auto-fill)

(global-set-key (kbd "C-x C-b") 'ibuffer)
;; org bullets
(require 'org-bullets)
(add-hook 'org-mode-hook (lambda () (org-bullets-mode 1)))
;;(setq debug-on-error 1)

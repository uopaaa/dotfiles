# Dotfiles
Do not forget to farm all those links :)
How ? 
	cd ~/dotfiles && stow */
To delete -> stow -D .
## Programs:
## Suckless Family
### Software that sucks less...
#### dwm
#### dmenu
#### slstatus
#### st
## Alacritty
## i3
### i3 config and i3status config
## Emacs
### init.el
## Bash
### .bashrc
## X stuff
### .Xresources (HiDPI setup)
### .xinitrc
### .xprofile
